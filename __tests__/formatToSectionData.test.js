import React from "react";
import formatToSectionData from "../utils/formatToSections";

const MOCK_DATA = [
  {
    transactionType: "Customer_Wallet_Debit_Fund_Transfer",
    dateOfTransaction: "2021-05-04",
    billingAmount: 65,
    billingCurrencyCode: "AUD",
    createdAt: "2021-05-04 03:51:34",
    labels: {
      receiverFirstName: "Peter",
      receiverLastName: "Pan",
    },
  },
  {
    transactionType: "Wallet_Fund_Transfer",
    dateOfTransaction: "2021-05-02",
    billingAmount: 85,
    billingCurrencyCode: "AUD",
    createdAt: "2021-05-02 11:52:18",
    labels: {
      receiverFirstName: "Bill",
      receiverLastName: "Doors",
    },
  },
];
describe("formatToSectionData", () => {
  it("should return null if the data was null or undefined", () => {
    const data = formatToSectionData({ data: null });
    expect(data).toBeNull();
  });
  it("should return null if the propertyName is empty", () => {
    const data = formatToSectionData({
      data: MOCK_DATA,
      propertyName: "",
    });
    expect(data).toBeNull();
  });

  it("should return null if the data is not an array", () => {
    const data = formatToSectionData({
      data: "MOCK_DATA",
      propertyName: "dateOfTransaction",
    });

    expect(data).toBeNull();
  });

  it("should return the formatted data if the data and propertyName are properly passed", () => {
    const data = formatToSectionData({
      data: MOCK_DATA,
      propertyName: "dateOfTransaction",
    });

    expect(data).toEqual([
      {
        data: [
          {
            billingAmount: 65,
            billingCurrencyCode: "AUD",
            createdAt: "2021-05-04 03:51:34",
            dateOfTransaction: "2021-05-04",
            labels: { receiverFirstName: "Peter", receiverLastName: "Pan" },
            transactionType: "Customer_Wallet_Debit_Fund_Transfer",
          },
        ],
        title: "2021-05-04",
      },
      {
        data: [
          {
            billingAmount: 85,
            billingCurrencyCode: "AUD",
            createdAt: "2021-05-02 11:52:18",
            dateOfTransaction: "2021-05-02",
            labels: { receiverFirstName: "Bill", receiverLastName: "Doors" },
            transactionType: "Wallet_Fund_Transfer",
          },
        ],
        title: "2021-05-02",
      },
    ]);
  });
  it("should format the title when the title formatter is passed", () => {
    const data = formatToSectionData({
      data: MOCK_DATA,
      propertyName: "dateOfTransaction",
      titleFormatter: (value) => value + "wow",
    });

    expect(data).toEqual([
      {
        data: [
          {
            billingAmount: 65,
            billingCurrencyCode: "AUD",
            createdAt: "2021-05-04 03:51:34",
            dateOfTransaction: "2021-05-04",
            labels: { receiverFirstName: "Peter", receiverLastName: "Pan" },
            transactionType: "Customer_Wallet_Debit_Fund_Transfer",
          },
        ],
        title: "2021-05-04wow",
      },
      {
        data: [
          {
            billingAmount: 85,
            billingCurrencyCode: "AUD",
            createdAt: "2021-05-02 11:52:18",
            dateOfTransaction: "2021-05-02",
            labels: { receiverFirstName: "Bill", receiverLastName: "Doors" },
            transactionType: "Wallet_Fund_Transfer",
          },
        ],
        title: "2021-05-02wow",
      },
    ]);
  });
  it("should still get the formatted data even if the titleFormatter is not passed", () => {
    const data = formatToSectionData({
      data: MOCK_DATA,
      propertyName: "dateOfTransaction",
      titleFormatter: (value) => value + "wow",
    });

    expect(data).not.toBe(null);
  });
});
