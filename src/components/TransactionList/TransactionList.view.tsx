import React, { FC } from "react";
import { SafeAreaView, SectionList, View, Text } from "react-native";
import { GenericSectionData } from "../../../utils/formatToSections";
import styles from "./TransactionList.style";
import { TransactionData } from "./types";

const Transaction: FC<{ item: TransactionData }> = ({ item }) => {
  return (
    <View style={styles.item}>
      <View style={styles.firstSection}></View>
      <View style={styles.midSection}>
        <Text style={styles.italicized}>
          {item.labels.receiverFirstName} {item.labels.receiverLastName}
        </Text>
      </View>
      <View>
        <Text style={styles.title}>${item.billingAmount}</Text>
        <Text style={styles.currency}>{item.billingCurrencyCode}</Text>
      </View>
    </View>
  );
};

const TransactionListView: FC<{
  sectionedTransactions: Array<GenericSectionData<TransactionData>>;
}> = ({ sectionedTransactions }) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.listContainer}>
        <SectionList
          sections={sectionedTransactions}
          keyExtractor={(item, index) => item.dateOfTransaction + index}
          renderItem={({ item }) => <Transaction item={item} />}
          renderSectionHeader={({ section: { title } }) => (
            <Text style={styles.sectionHeader}>{title || ""}</Text>
          )}
          stickySectionHeadersEnabled={false}
        />
      </View>
    </SafeAreaView>
  );
};

export default TransactionListView;
