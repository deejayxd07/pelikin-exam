export interface TransactionData {
  transactionType: string;
  dateOfTransaction: string;
  billingAmount: number;
  billingCurrencyCode: string;
  createdAt: string;
  labels: {
    receiverFirstName: string;
    receiverLastName: string;
  };
}

export interface SectionedTransaction {
  title: string;
  data: Array<TransactionData>;
}
