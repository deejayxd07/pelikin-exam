import moment from "moment";
import React, { FC, useEffect, useState } from "react";
import data from "../../../data/pelikin.json";
import TransactionListView from "./TransactionList.view";
import * as R from "ramda";
import { SectionedTransaction, TransactionData } from "./types";
import formatToSectionData, {
  GenericSectionData,
} from "../../../utils/formatToSections";

const TransactionContainer = () => {
  const [sectionedTransactions, setSectionedTransactions] = useState<
    Array<GenericSectionData<TransactionData>>
  >([]);

  useEffect(() => {
    const formattedTransactionData: Array<
      GenericSectionData<TransactionData>
    > | null = formatToSectionData({
      data: data.transactions,
      propertyName: "dateOfTransaction",
      titleFormatter: (value) => moment(new Date(value)).format("MMM Do YY"),
    });

    if (formattedTransactionData) {
      setSectionedTransactions(formattedTransactionData);
    }
  }, []);

  return <TransactionListView sectionedTransactions={sectionedTransactions} />;
};

export default TransactionContainer;
