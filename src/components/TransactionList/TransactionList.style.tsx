import { StyleSheet, TextStyle } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";

const DEFAULT_FONT_COLOR = "rgb(255,255,255)";
const BG_COLOR = "rgb(38, 84, 171)";

const BOLD_FONT_STYLE: TextStyle = {
  fontSize: RFValue(14),
  fontWeight: "bold",
  color: DEFAULT_FONT_COLOR,
  fontFamily: "Raleway_700Bold",
};

const HIGHLIGHT_COLOR = "rgb(102, 152, 220)";

const ITALICIZED_FONT_STYLE: TextStyle = {
  fontSize: RFValue(12),
  color: DEFAULT_FONT_COLOR,
  fontFamily: "Raleway_700Bold_Italic",
  textTransform: "uppercase",
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BG_COLOR,
    width: "100%",
  },
  listContainer: {
    paddingHorizontal: 20,
  },
  sectionHeader: {
    ...BOLD_FONT_STYLE,
    fontSize: RFValue(14),
    color: HIGHLIGHT_COLOR,
  },

  item: {
    padding: 14,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255, 0.1)",
    borderRadius: 10,
    justifyContent: "space-between",
    marginVertical: 10,
  },

  firstSection: {
    flex: 0.2,
  },
  midSection: {
    flex: 1,
  },

  title: BOLD_FONT_STYLE,

  italicized: {
    ...ITALICIZED_FONT_STYLE,
  },
  currency: {
    ...ITALICIZED_FONT_STYLE,
    marginTop: 5,
    color: HIGHLIGHT_COLOR,
  },
});

export default styles;
