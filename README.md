### Pelikin Exam

![Alt text](screenshot.png "Screenshot")

### Requirements

- [Node](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)

### Installation Steps

1. Clone the repo
2. Install dependencies by running `yarn`
3. Install pod dependencies by running `yarn pod`

## Running the app

### iOS

`yarn ios`

### Android

`yarn android`

## Running the tests

`yarn test`
