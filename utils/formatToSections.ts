import * as R from "ramda";

interface FormatterParams {
  data: Array<Record<string, any>>;
  propertyName: string;
  titleFormatter?: (propertyName: string) => void;
}

export interface GenericSectionData<T> {
  data: Array<T>;
  title: string | void;
}

function formatToSectionData<T>({
  data,
  propertyName,
  titleFormatter,
}: FormatterParams): Array<GenericSectionData<T>> | null {
  if (!data || !Array.isArray(data) || !propertyName) {
    return null;
  }

  var newData: Record<any, any> = R.groupBy(R.prop(propertyName), data);

  const formattedTransactionData: Array<GenericSectionData<T>> = [];

  for (const [key, value] of Object.entries(newData)) {
    formattedTransactionData.push({
      title: titleFormatter ? titleFormatter(key) : key,
      data: value,
    });
  }

  return formattedTransactionData;
}

export default formatToSectionData;
